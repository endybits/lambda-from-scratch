import json
import requests

def lambda_handler_stg1(event, context):
    return {
        'statusCode': 200,
        'body': json.dumps('Hello from a zipped file in Lambda!')
    }

def lambda_handler(event, context):
    response = requests.get('https://jsonplaceholder.typicode.com/todos/1')
    return {
        'statusCode': response.status_code,
        'body': response.json()
    }