# 🚀 lambda-from-scratch

Welcome to the **lambda-from-scratch** hands-on project! This project is designed to help you understand and deploy AWS Lambda step by step, starting from the basics and gradually building up to more complex projects. 🛠️

## Project Stages

### Stage 1: Simple Python File Execution
In this stage, you will learn how to execute a simple Python file using AWS Lambda. 🐍🚀

#### Step 1: Create a `main.py` File

In this initial step, let's get started by creating a simple `main.py` file. We'll include a basic Lambda handler function to greet the world.

1. Create a `main.py` file in your project directory.
2. Add the following code to `main.py`:

```python
import json

def lambda_handler(event, context):
    return {
        'statusCode': 200,
        'body': json.dumps('Hello from a zipped file in Lambda!')
    }
```
#### Step 2: Package the Python File in a Zip
Now that you have your main.py ready, it's time to package it into a zip file that can be deployed to AWS Lambda. Let's proceed!

1. Create a zip file containing your main.py and any necessary dependencies. In my case, I used this code `zip my-first-lambda-deployment.zip main.py`
2. Ensure that your zip file includes only the required files and folders.
3. You're now prepared to deploy your Lambda function with the packaged zip file.

#### Step 3: Create a Lambda Function on AWS

Now that you have your Lambda function code packaged, you can create a Lambda function on AWS. Here's how:

1. Log in to your AWS Management Console.
2. Navigate to the AWS Lambda service.
3. Click the "Create function" button.
4. Choose "Author from scratch."
5. Fill in the basic details:
   - **Function name**: Give your Lambda function a unique name.
   - **Runtime**: Select the runtime that matches your Python version (e.g., Python 3.10).
   - **Execution role**: You can either choose an existing role or create a new one with the necessary permissions. (AWS provides you a default execution role)
6. Click the "Create function" button.

#### Step 4: Upload the Zip Package

Now it's time to upload the zip package containing your Lambda function code:

1. Scroll down to the "Function code" section.
2. In the "Code entry type", select the "Code" tab.
3. Click the "Upload" button and select the zip file you created in Step 2.
4. Once the zip file is uploaded, AWS Lambda will automatically extract the code.
5. Ensure that the "Handler" field is set to `main.lambda_handler` (assuming your Python file is named `main.py` and contains a function named `lambda_handler`). Edit the Handler scrolling down until "Runtime settings".

#### Step 5: Test Your Lambda Function

Before deploying your Lambda function, it's essential to test it:

1. Scroll to the top of the Lambda configuration page.
2. Click the "Test" button.
3. Create a test event or select an existing one.
4. Click the "Test" button again to execute your Lambda function.
5. Review the results in the "Execution result" section.

Congratulations! You've successfully created and executed a Lambda function on AWS. In the coming stages we will deploy it, configure more complex settings, and trigger Lambda function using various AWS services or integrate it into your applications. 🚀

Feel free to adapt these steps to your specific use case and customize the Lambda function as needed.


### Stage 2: Python File with Dependencies 🐍📦🚀

In this stage, we will enhance our Lambda function by adding external dependencies. This allows us to create more powerful and versatile serverless applications. Here's how to proceed:

#### Step 1: Update Your Python File

1. Open your `main.py` file (or the Python file containing your Lambda function code). This is my new code:
```python
import json
import requests

def lambda_handler(event, context):
    response = requests.get('https://jsonplaceholder.typicode.com/todos/1')
    return {
        'statusCode': response.status_code,
        'body': response.json()
    }
```

2. Import any external libraries or dependencies your function requires. Make sure to specify the required version of each library in your `requirements.txt` file.

3. Modify your Lambda function code to utilize these dependencies effectively. Test your code locally to ensure it works as expected.

#### Step 2: Package the Updated Code and Dependencies 📦

To ensure your Lambda function includes both the updated code and its dependencies, you can organize them into a folder structure and package them into a .zip file. Here's how to do it:

1. Inside your root project folder, create a subfolder named `dependencies`.

2. Use a tool like `pip` to install the required Python libraries into the `dependencies` folder. You can achieve this by running the command `pip install -t dependencies/ -r requirements.txt`. This is my resultant project structure:
```
.
├── README.md
├── dependencies
│   ├── bin
│   ├── certifi
│   ├── certifi-2023.7.22.dist-info
│   ├── charset_normalizer
│   ├── charset_normalizer-3.3.2.dist-info
│   ├── idna
│   ├── idna-3.4.dist-info
│   ├── requests
│   ├── requests-2.31.0.dist-info
│   ├── urllib3
│   └── urllib3-2.0.7.dist-info
├── main.py
├── requirements.txt
```

3. On yo ur project root, run the command `(cd dependencies; zip ../my-lambda-artifact-with-dependencies.zip -r .)` to zip the dependencies into the our artifact, then run the command `zip my-lambda-artifact-with-dependencies.zip -u main.py` to include the `main.py`.

#### Step 3: Deploy the Enhanced Lambda Function

1. Proceed to **Step 3** of **Stage 1** to create a Lambda function on AWS with your enhanced code package.

#### Step 4: Configure Dependencies in AWS Lambda

1. In the AWS Lambda console, navigate to your Lambda function's configuration.

2. In the "Function code" section, ensure that the uploaded .zip file includes your updated code and dependencies.

3. Verify that the "Handler" field still points to `main.lambda_handler` or your function's entry point.

#### Step 5: Test Your Enhanced Lambda Function

1. To validate your Lambda function's enhanced capabilities, follow the testing procedure described in **Step 5** of **Stage 1**.
I got this output response:
```json
{
  "statusCode": 200,
  "body": {
    "userId": 1,
    "id": 1,
    "title": "delectus aut autem",
    "completed": false
  }
}
```

With your Lambda function now equipped with external dependencies, you can build more sophisticated serverless applications and execute complex tasks efficiently.


### Stage 3: FastAPI Application Deployment
In the final stage, we'll deploy a FastAPI application to AWS Lambda. 🚀🌐
This will allow you to create web APIs and services that can scale effortlessly in the cloud.

#### Step 1: Create Your FastAPI Application

1. Develop your FastAPI application in a Python script. Here's a simple example of a FastAPI app:
```python
from fastapi import FastAPI
from mangum import Mangum

app = FastAPI()
handler = Mangum(app)

@app.get("/")
def read_root():
    return {"Hello": "World world! I am running inside a container in Lambda!"}
```
2. Organize your FastAPI project and ensure it includes all necessary dependencies and configuration files.

#### Step 2: Package Your FastAPI Application

1. Create a zip file containing your FastAPI application code and any required dependencies: 
    - Use a tool like `pip` to install the required Python libraries into the `api_dependencies` folder. You can achieve this by running the command `pip install --target=api_dependencies -r requirements.txt`
    - Run the command `(cd api_dependencies; zip ../fastapi_artifact.zip -r .)` to zip the dependencies into the our artifact, then run the command `zip fastapi_artifact.zip -u -r app` to include FastAPI app too.
2. Make sure the zip file structure is well-organized, including all necessary files and folders.

#### Step 3: Create a Lambda Function for Your FastAPI App

1. Log in to your AWS Management Console.
2. Navigate to the AWS Lambda service.
3. Click the "Create function" button.
4. Choose "Author from scratch."
5. Fill in the basic details:
   - **Function name**: Give your Lambda function a unique name.
   - **Runtime**: Select the Python runtime that matches your FastAPI app's requirements.
   - **Execution role**: Choose or create an execution role with the necessary permissions.
6. Go to **Advanced setting**, select "Enable function URL "to assign HTTP(S) endpoints to your Lambda function", then select "NONE" option for Auth type.
7. Click the "Create function" button.

#### Step 4: Upload Your FastAPI Application

Now, it's time to upload the zip package containing your FastAPI application to the Lambda function.

1. Scroll down to the "Function code" section in your Lambda function's configuration.

2. In the "Code entry type" dropdown, select "Upload a .zip file."

3. Click the "Upload" button and choose the zip file you created in Step 2.

4. Once the zip file is uploaded, AWS Lambda will automatically extract the code.

5. Ensure that the "Handler" field is set correctly. It should point to the entry point of your FastAPI application within the zip package. Considering the structure of my project my Handler is `app.main.handler`.

#### Step 5: Configure Your FastAPI Lambda Function
1. Customize the configuration settings for your Lambda function, including memory, timeout, and environment variables, as needed to accommodate your FastAPI app's requirements.
2. Ensure that your FastAPI app is designed to work well in a serverless environment, handling concurrent requests effectively.

#### Step 6: Testing and Deployment
1. Test your FastAPI Lambda function by creating a test event and executing it. In my case I used directly the URL *(remember avanced setting: **step 3 on this stage***) 
2. Monitor and troubleshoot any issues that may arise during testing.
3. Once your FastAPI app works as expected, you're ready to deploy it as a serverless API.

Congratulations! You've successfully deployed a FastAPI application on AWS Lambda, creating a highly scalable and efficient web API. You can now integrate this serverless API into your web or mobile applications, providing fast and reliable services to your users. 🚀🌐


### Stage 4: Automated Deployment with CI/CD
In this advanced stage, you'll set up automated deployment using Continuous Integration/Continuous Deployment (CI/CD). Streamline your development workflow and deploy with ease! 🤖🚀

## What's Next?

Customize this README to fit your project's needs, and enjoy the journey of deploying AWS Lambda functions! 🌟
